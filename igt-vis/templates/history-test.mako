<% from common import htmlname, ttip, htmlttip, boxedletter %>

<%inherit file="common.mako"/>
<%namespace name="helpers" file="helpers.mako"/>

<%block name="title">${test} CI History</%block>

<table>
  <tr>
    <th></th>
    % for build in builds:
      <th><span class="v">
	<a href="${path}${build}/filelist.html">${build}</a>
      </th>
    % endfor
  </tr>
  % for host in hosts:
    <tr>
      <td class="h"><a href="${host}.html">${host}</a></td>
      % for build in builds:
	<%
	  try: hostname = jsons[(build, host)]['tests'][test]['hostname']
	  except: hostname = host
	  try:
	      node = jsons[(build, host)]['tests'][test]
	      res,tooltip = ttip(node)
	      letter = boxedletter(node)
	      link = path+"testresult.html"+"#"+build+"/"+hostname+"/results" + jsons[(build, host)]['tests'][test]['run'] + ".json.bz2#"+test
	  except:
	      res,tooltip = ('notrun', "")
	      letter = '&nbsp;'
	      link = ""
	  tooltip=htmlttip(tooltip)
	%>
	${helpers.result_cell(res, link, letter, test, build, tooltip)}
      % endfor
    </tr>
  % endfor
</table>

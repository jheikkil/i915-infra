# Intel GFX CI


## What Is It About?

We are continously testing [i915][i915-desc] (a Linux kernel device driver
for Intel Graphics) in an automated fashion, with a goal of having production
ready upstream.

## Hardware List

Our CI system is composed of multiple Intel machines spanning many
generations and display types. You can find our full hardware list
[here](/hardware.html).

## Pre-Merge Testing

**This is the crux of this CI system.** We test each patch that goes in our
driver ([i915][i915-desc]) or in the DRM drivers test suite we use ([IGT GPU
Tools][igt-desc]) before it lands in the repository and compare the results
with the [post-merge baseline](#git-trees-tested-post-merge) ([we filter out
known bugs](#results-filtering-and-bug-tracking)). This shifts the cost of
integration on the people making the change and helps us avoid time-consuming
bisection and reverts.

[i915-desc]:      https://01.org/linuxgraphics/gfx-docs/drm/gpu/i915.html
[igt-desc]:       https://gitlab.freedesktop.org/drm/igt-gpu-tools#igt-gpu-tools

Since we accept patches through mailing lists, this is where you can find the
results - they are sent out as a replies to the original mail. We run
[BAT](#basic-acceptance-tests-aka-bat) and [FULL
IGT](#full-igt-aka-sharded-runs) (if BAT is successful). On a usual day you
can expect result emails for a BAT within 1 hour and for FULL IGT within 6
hours. You will receive the emails even if the run is successful.

Here are the mailing lists we currently support:

| Mailing List                           | List Info              | Archive                   | [Patchwork][patchwork-desc]                                      |
| --                                     | --                     | --                        | --                                                               |
| intel-gfx@lists.freedesktop.org        | [info][intel-gfx-info] | [archive][intel-gfx-arch] | [patchwork][intel-gfx-pw]                                        |
| igt-dev@lists.freedesktop.org          | [info][igt-dev-info]   | [archive][igt-dev-arch]   | [patchwork][igt-dev-pw]                                          |
| intel-gfx-trybot@lists.freedesktop.org | [info][trybot-info]    | [archive][trybot-arch]    | [kernel patchwork][trybot-pw]<br/>[IGT patchwork][trybot-igt-pw] |

And [here is a guide for reading pre-merge results emails](/pre-merge-results.md).


[intel-gfx-info]: https://lists.freedesktop.org/mailman/listinfo/intel-gfx
[intel-gfx-arch]: https://lists.freedesktop.org/archives/intel-gfx/
[intel-gfx-pw]:   https://patchwork.freedesktop.org/project/intel-gfx/series/

[igt-dev-info]:   https://lists.freedesktop.org/mailman/listinfo/igt-dev
[igt-dev-arch]:   https://lists.freedesktop.org/archives/igt-dev/
[igt-dev-pw]:     https://patchwork.freedesktop.org/project/igt/series/

[trybot-info]:    https://lists.freedesktop.org/mailman/listinfo/intel-gfx-trybot
[trybot-arch]:    https://lists.freedesktop.org/archives/intel-gfx-trybot/
[trybot-pw]:      https://patchwork.freedesktop.org/project/intel-gfx-trybot/series/
[trybot-igt-pw]:  https://patchwork.freedesktop.org/project/igt-trybot/series/

[patchwork-desc]: https://gitlab.freedesktop.org/patchwork-fdo/patchwork-fdo#patchwork-fdo


### Trybot

Trybot mailing list is for trying out changes on our infrastracture before
they are ready for a review. It has the lowest priority of all the mailing
lists. It supports both IGT and kernel patches.

IGT patches sould have `i-g-t` tag in the subject, e.g. `[PATCH i-g-t] My
patch`. This can be automated with: <br/>
`git config --local format.subjectPrefix "PATCH i-g-t"`

### Queues

You can check our pre-merge queues for [BAT](#basic-acceptance-tests-aka-bat)
and [Full/Sharded](#full-igt-aka-sharded-runs). They are helpful for
assessing how busy the CI is. See [Queues](/queue/index.html) for
details.

If you are interested in knowning the testing latency's distribution, you
should check out the [dedicated latency page](/latency.html).

### Forcing Tests In BAT And Changing Configuration

**To force some IGT tests** in CI you just need to add an extra patch to your
series that makes the desired alteration to
`tests/intel-ci/fast-feedback.testlist` file - CI uses it directly.

Make sure to mark such patch by adding `HAX` in the subject, and if not
obvious - explain the reason for the forced testing in the commit message.
[Example.][force-test-patch]

**To change kernel's configuration** or command line parameters just submit a
patch that changed the relevant part (e.g. `Kconfig` or `i915_params.h`) and
submit it as an extra patch with `HAX` in the subject.
[Example.][i915-force-option-patch]

To test force IGT test list changes for a kernel patch series you have to
apply the above to our guide on [Testing Combined IGT And Kernel
Changes](/test-with.md).


[force-test-patch]: https://lists.freedesktop.org/archives/intel-gfx-trybot/2019-September/081103.html
[force-test-results]: https://intel-gfx-ci.01.org/tree/drm-tip/TrybotIGT_44/bat-all.html?testfilter=_fill&hosts=fi-tgl-u
[i915-force-option-patch]: https://lists.freedesktop.org/archives/intel-gfx/2019-September/211732.html


### Testing Combined IGT And Kernel Changes
See [Testing Combined IGT And Kernel Changes](/test-with.md) for details.

## Git Trees Tested Post-Merge

We test many trees post-merge. Some of them are our baseline for pre-merge
testing (drm-tip and IGT GPU Tools), while the others helps us to make sure
that what we submit upstream works and catch any potential issues cased by
changes in other drivers/trees before we actually integrate with them.

The testing is sparse, i.e. we poll the branch for changes periodically, and
if something has changed we run it through our CI, even if that means
multiple commits/merges.

| CI Results                                 | Documentation          | Repository                       |
| --                                         | --                     | --                               |
| [drm-tip][]                                | [docs][drm-tip-doc]    | [repo][drm-tip-repo]             |
| [IGT GPU Tools][drm-tip]<sup>\*</sup>      | [docs][igt-doc]        | [repo][igt-repo]                 |
| [Linus' tree][linus]                       | ???                    | [repo][linus-repo]               |
| [linux-next][]                             | [docs][linux-next-doc] | [repo][linux-next-repo]          |
| [drm-intel-fixes][drm-intel-fixes]         | [docs][drm-intel-doc]  | [repo][drm-intel-fixes-repo]     |
| [drm-intel-next-fixes][dinf]               | [docs][drm-intel-doc]  | [repo][dinf-repo]                |
| [drm-intel-next][din]                      | [docs][drm-intel-doc]  | [repo][din-repo]                 |
| [drm-intel-gt-next][dign]                  | [docs][drm-intel-doc]  | [repo][dign-repo]                |
| [drm-misc-fixes][drm-misc-fixes]           | [docs][drm-misc-doc]   | [repo][drm-misc-fixes-repo]      |
| [drm-misc-next-fixes][drm-misc-next-fixes] | [docs][drm-misc-doc]   | [repo][drm-misc-next-fixes-repo] |
| [drm-intel-media][drm-intel-media]         | ???                    | [repo][drm-intel-media-repo]     |
| [drm-next][drm-next]                       | ???                    | [repo][drm-next-repo]            |

*\*: IGT results are part of the drm-tip visualisation*

[drm-tip]:                   /tree/drm-tip/index.html
[drm-tip-repo]:              https://cgit.freedesktop.org/drm-tip
[drm-tip-doc]:               https://drm.pages.freedesktop.org/maintainer-tools/drm-tip.html

[igt-repo]:                  https://gitlab.freedesktop.org/drm/igt-gpu-tools
[igt-doc]:                   https://gitlab.freedesktop.org/drm/igt-gpu-tools#igt-gpu-tools

[linus]:                     /tree/linus/index.html
[linus-repo]:                https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

[linux-next]:                /tree/linux-next/index.html
[linux-next-repo]:           https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/
[linux-next-doc]:            https://www.kernel.org/doc/man-pages/linux-next.html

[drm-intel-fixes]:           /tree/drm-intel-fixes/index.html
[drm-intel-fixes-repo]:      https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-fixes
[dinf]:                      /tree/drm-intel-next-fixes/index.html
[dinf-repo]:                 https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-next-fixes
[din]:                       /tree/drm-intel-next/index.html
[din-repo]:                  https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-next
[dign]:                      /tree/drm-intel-gt-next/index.html
[dign-repo]:                 https://cgit.freedesktop.org/drm-intel/log/?h=drm-intel-gt-next
[drm-intel-doc]:             https://drm.pages.freedesktop.org/maintainer-tools/repositories.html#the-upstream-i915-driver-repository

[drm-misc-fixes]:            /tree/drm-misc-fixes/index.html
[drm-misc-fixes-repo]:       https://cgit.freedesktop.org/drm-misc/log/?h=drm-misc-fixes
[drm-misc-next-fixes]:       /tree/drm-misc-next-fixes/index.html
[drm-misc-next-fixes-repo]:  https://cgit.freedesktop.org/drm-misc/log/?h=drm-misc-next-fixes
[drm-misc-doc]:              https://drm.pages.freedesktop.org/maintainer-tools/drm-misc.html

[drm-intel-media]:           /tree/drm-intel-media/index.html
[drm-intel-media-repo]:      https://cgit.freedesktop.org/~tursulin/drm-intel/?h=media

[drm-next]:                  /tree/drm-next/index.html
[drm-next-repo]:             https://cgit.freedesktop.org/drm/drm?h=drm-next

### Repositories With CI Tags

 * **Kernel**: `git://anongit.freedesktop.org/gfx-ci/linux`
 * **IGT**: `https://gitlab.freedesktop.org/gfx-ci/igt-ci-tags.git`

Try `git ls-remote` and don't forget to fetch tags with `git fetch -t`!

## Results Filtering And Bug Tracking

Bugs caught by our system are filled to the following bug trackers:

 * freedesktop's gitlab for [kernel][kernel-issues] and [IGT][igt-issues]
   issues
 * [kernel.org's bugzilla][korg-bugzilla] - all other upstream bugs

[kernel-issues]: https://gitlab.freedesktop.org/drm/intel/issues
[igt-issues]:    https://gitlab.freedesktop.org/drm/igt-gpu-tools/issues
[korg-bugzilla]: https://bugzilla.kernel.org/

We also maintain a set of filters that tie those bugs to failures we see in
our CI using machine names/types and patterns in dmesg/test output. This way
we are able to filter known issues out of pre-merge results to decrease noise
for developers, keep track of bug's life cycle, and reproduction rate. The
tool makes us able to confirm that a supposed fix is indeed fixing things.

You will be able to browse the CI issues with the related data using our tool
called cibuglog once we will have it deployed publicly. In the meantime you
can browe [its source code][cibuglog-source].

[cibuglog-source]: https://gitlab.freedesktop.org/gfx-ci/cibuglog

## The Kinds Of Runs

### Basic Acceptance Tests (aka BAT)

BAT is the most basic run in our portfolio - it consists of tests that help
us ensure that the testing configuration is in a working condition. It gates
all the sharded runs - if it breaks, then the build is deemed to broken to
use more of the CI time on it. It utilizes [fast-feedback.testlist][], which
you can find in the IGT repository.

[fast-feedback.testlist]: https://gitlab.freedesktop.org/drm/igt-gpu-tools/blob/master/tests/intel-ci/fast-feedback.testlist

### Full IGT (aka sharded runs)

If the BAT run is succesful, then we continue with the sharded run. Much
broader set of tests (everything you can find in IGT filtered through our
[blacklist.txt][]) is executed.

[blacklist.txt]: https://gitlab.freedesktop.org/drm/igt-gpu-tools/blob/master/tests/intel-ci/blacklist.txt


### Idle Runs

Those runs are complementary to the ones above, used mainly to gather extra
data and increase coverage. As the name suggests, they are run when CI would
be idle otherwise (i.e. there nothing to test for the regular
pre-merge/post-merge).

- [drmtip][drmtip-results] - Full IGT but on the same hosts that BAT uses,
  thus increasing coverege.
- [KASAN][kasan-results] - same as above but runs with
  [The Kernel Address Sanitizer][kasan] enabled.
- 100 re-runs - re-run BAT 100 times with fixed IGT and kernel for extra data
  on flip-floppers.

[drmtip-results]: /tree/drm-tip/drmtip.html
[kasan-results]:  /tree/drm-tip/kasan.html

[kasan]: https://www.kernel.org/doc/html/latest/dev-tools/kasan.html

### Other Runs

**Resume runs** are using the same test list as Full IGT but are done on a
single machine. In case of hangs/incompletes the run is resumed. They are not
a part of regular CI and are tiggered on demand.

## Contacts

 * **IRC:** #intel-gfx-ci @ [OFTC](https://www.oftc.net/)
 * **Mailing list:** [i915-ci-infra @ lists.freedesktop.org](https://lists.freedesktop.org/mailman/listinfo/i915-ci-infra)
 * **hardware/CI maintainer:** Tomi Sarvela - tomi.p.sarvela @ intel.com
 * **cibuglog/metrics maintainer:** Karol Krol - karol.krol @ intel.com

# Testing Combined IGT And Kernel Changes

Our CI allows for combined testing of IGT and kernel changes. See [Pre-Merge
Testing](/#pre-merge-testing) for details about the mailing lists.


## Cover Letter

IGT changes have to be sent first. When sending the kernel patch(es)
(both intel-gfx and trybot mailing lists are supported)  make sure that you
have a **cover letter** that includes `Test-with: MSG-ID`. MSG-ID has to
point to any patch in the IGT series. MSG-ID can be provided either with or
without the `<>`.

**Example:** <https://lists.freedesktop.org/archives/intel-gfx/2019-February/190638.html>

     Test-with: 1551165805-19130-2-git-send-email-ramalingam.c@intel.com

**Note:** Please make sure that you have patch numbers in the subject for
cover letter and all the patches (e.g. `[PATCH 0/1]`). Older versions of git
do not add those for series with just a single patch. Patchwork uses "patch
number 0" to tell that the mail is a cover letter.

**Warning:** Don't test-with old series. The historical builds artifact are
being removed aggressively. As of now if you test-with anything older than a
day the behavior is undefined.

## Results

If the `Test-with:` has been picked up correctly you should see, near the
bottom of the results email, that both IGT and Linux components were changed:


**Example:** <https://lists.freedesktop.org/archives/intel-gfx/2019-February/190656.html>

     Build changes
     -------------
     
         * IGT: IGT_4854 -> IGTPW_2521
         * Linux: CI_DRM_5659 -> Patchwork_12303

## Tips

You can make sure that your MSG-ID resolves correctly by using Patchwork:
`https://patchwork.freedesktop.org/patch/msgid/MSG-ID`. This should redirect
you to that particular patch. (e.g.
<https://patchwork.freedesktop.org/patch/msgid/1551165805-19130-2-git-send-email-ramalingam.c@intel.com>)

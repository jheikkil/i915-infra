var lasttime = 0.0;
var timeshow = 0;
var messagetoggle = 0;
var highlighteri = 0;
var tooltipperi = 0;
var highlightwords = "";
var allowprocessing = 0;
var flail = null;

var stateperline = [];
var state = {};
var dmesgdivi = null;

var generalprocessingblock = false;

async function decompbz2(bits){
    return new Promise((resolve) => {
        var size;
        try {
            size = bzip2.header(bits);
        } catch (e) {
            resolve(bits);
        }

        var all = '', chunk = '';

        const nIntervId = setInterval(() => {
            all += chunk;
            chunk = bzip2.decompress(bits, size);

            if (chunk == -1) {
                clearInterval(nIntervId);
                resolve(all);
            }
        }, 1);
    });
}

var getFileCached = async function (file) {
    if (newCache == null)
        return(undefined);

    const response = await newCache.match(file);

    if (typeof response != 'undefined') {
        text = await response.text();
        writejsoncache(file, text.length, text);
        return(JSON.parse(text));
    } else {
        return(undefined);
    }
}

var getFileHttp = function (file, storecache=true) {
    return new Promise((resolve, reject) => {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.responseType = storecache ? "arraybuffer" : "text";

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState != 4)
                return;

            if (xmlhttp.status >= 200 && xmlhttp.status < 300) {

                if (storecache) {
                    var bytes = new Uint8Array(xmlhttp.response);
                    decompbz2(bzip2.array(bytes)).then(stream => {
                        writejsoncache(file, stream.length, stream);
                        resolve(JSON.parse(stream))});
                } else
                    resolve(xmlhttp.responseText);
            } else {
                reject({
                    status: xmlhttp.status,
                    statusText: xmlhttp.statusText
                });
            }
        };

        xmlhttp.ontimeout = function () {
            console.log('timeout');
            reject('timeout')
        };

        xmlhttp.open('GET', file, true);
        xmlhttp.send();
    });
}


async function getFile(filename) {
    let file = await getFileCached(filename);
    if (typeof file == 'undefined') {
        file = await getFileHttp(filename);
    }
    return file;
}


async function fetchfile(file) {
    try {
        if (loadedfiles[file] == undefined) {
            loadedfiles[file] = {json:await getFile(file)};
        }
    } catch(err) {
        console.log("fetching file " + file + " failed.\n" + err.status + "\n" + err.statusText);
    }
}
////

function getlineparts(line)
{
    var c = 0;
    var clean = []

    line.split(/([\[\]])/).filter(Boolean).forEach(
        e => e == '\[' ? c++ : e == '\]' ?
            c-- : c == 2 ?
                clean.push("") : clean.push(e)
    );

    let parts = clean.join('').replace(/\s+/g,' ').trim().split(' ');

    if (parts.length > 2)
    {
        if (parts[2] == "i915") {
            if (parts[4].startsWith("drm:"))
                parts[4] = parts[4].slice(4);

            parts.splice(2, 2);
        } else if (parts[2].startsWith("drm:")) {
            parts[2] = parts[2].slice(4);
            if (parts[3] == "i915")
                parts.splice(3, 1);
        }
    }
    return parts
}

/**
 * from https://stackoverflow.com/a/31102605
 */
function sortObject(unordered) {
    const ordered = Object.keys(unordered).sort().reduce(
        (obj, key) => {
          obj[key] = unordered[key];
          return obj;
        },
        {}
      );
    return ordered;
}

function formatcontent(jasoni)
{
    let usedlist = [];
    let collectionlist = ["CONNECTOR", "CRTC", "OTHER"];
    rval = "";

    if (generalprocessingblock) {
        return "<div class=\"rdiv\"><h2 class=\"tooltiptable\">Processing disabled for big dmesg</h2>";
    }

    for (var n = 0; n < collectionlist.length; n++) {
        switch (collectionlist[n]) {
        case "CONNECTOR":
            var contablewritten = false;

            Object.entries(jasoni).forEach(([key, value]) => {
                if (key.startsWith(collectionlist[n])) {
                        if (contablewritten == false) {
                            contablewritten = true;
                            rval += "<div class=\"rdiv\"><h2 class=\"tooltiptable\">Connectors:</h2><table class=\"tooltiptable\">";
                        }

                        rval += "<tr><td>" + key.split(':')[2]  + "&#9;</td><td>"+ value + "</td></tr>";
                        usedlist.push(key);
                }
            });

            if (contablewritten == true) {
                rval += "</table></div>";
            }
        break;
        case "CRTC":
            Object.entries(jasoni).forEach(([key, value]) => {
                if (key.startsWith(collectionlist[n])) {

                    rval += "<div class=\"rdiv\"><h2  class=\"tooltiptable\" style=\"white-space:normal;\">" + key + ": " + value + "</h2>";

                    var tablewritten = false;
                    Object.entries(jasoni).forEach(([planekey, planevalue]) => {
                        if (planekey.startsWith("PLANE" +  key.split(' ')[2])) {
                            if (tablewritten == false) {
                                tablewritten = true;
                                rval += "<table class=\"tooltiptable\">";
                            }
                            planevalue.replace("format", "\nformat").replace("little-endian", "\nlittle-endian").replace("modifier", "\nmodifier");
                            rval += "<tr><td style=\"white-space:pre;\">" + planekey.split(' ')[1] + " " + planekey.split(' ')[2] + "&#9;</td>";
                            rval += "<td>"+ planevalue + "</td></tr>";
                            usedlist.push(planekey);
                        }});

                    if (tablewritten == true) {
                        rval += "</table>";
                    }
                    rval += "</div>";
                    usedlist.push(key);
                }});
                break;
        case "OTHER":
            var tablewritten = false;

            Object.entries(jasoni).forEach(([key, value]) => {
                if (!usedlist.includes(key)) {
                    if (tablewritten == false) {
                        tablewritten = true;
                        rval += "<div class=\"rdiv\"><h2 class=\"tooltiptable\">Misc:</h2><table class=\"tooltiptable\">";
                    }
                    usedlist.push(key);
                    let temp = key.split(" ");
                    temp.shift();
                    temp = temp.join(" ");
                    rval += "<tr><td>" + temp + "&#9;</td><td>"+ value + "</td></tr>";
                }
            });

            usedlist.push(collectionlist[n]);
            if (tablewritten == true) {
                rval += "</table></div>";
            }
            break;
        }
    }
    return rval;
}

function highlighter(line)
{
    if (highlighteri == 0 || highlightwords.trim().length == 0)
        return line;

    let words = [];
    var re = /(\/[^\s]*\/(?:[gmi]){0,}($|\s))|([^\/\s][^\s]*)/gi;

    do {
        m = re.exec(highlightwords);
        if (m) {
            let thisword = m[0].trim();

            if (thisword[0] != '/')
                thisword = '/' + thisword + '/g';

            words.push(thisword);
        }
    } while (m);

    const colors = ["red", "white", "Sienna", "blue", "Orchid", "cyan"];

    let rval = line;
    let matchcount = 0;

    try {
        words.forEach((word, i) => {
            let matches = line.matchAll(eval(word));

            for (const match of matches) {
                if (match[0].length > 2)
                    rval = rval.replace(match, '<span class="innerspan" style="color:' + colors[i%colors.length] + '"><b class="bstyle">' + match + '</b></span>');

                if (++matchcount > 3)
                    throw new Error("too many matches per line");
            }
        });
    } catch(e) {
        rval = line;
    }

    return rval;
}

var ttip = {self:null, text: "", timeout:0, shown:false};

function tooltipping(owner, text) {
    let div = document.getElementById("tooltip");

    this.show = function(e) {
      if (tooltipperi == 0)
        return;

      if (div.innerHTML != text)
          div.innerHTML = text;

      ttip.shown = true;
      div.style.left = e.clientX + "px";
      div.style.top = e.clientY + "px";
      div.style.display = "block";

      let coordx = e.clientX;
      let coordy = e.clientY;
      let dw = div.clientWidth;
      let dh = div.clientHeight;

      if (coordx + dw > window.innerWidth)
          div.style.left = (coordx - (dw + 24)) + "px";

      if (coordy + dh > window.innerHeight)
          div.style.top = (coordy - (dh + 24)) + "px";

      ttip.timeout = setTimeout(this.hide, 15000);
    }

    this.hide = function() {
      ttip.shown = false;
      clearTimeout(ttip.timeout);
      try {
        div.style.display = "none";
      }
      catch (err) {}

      ttip.self = null;
    }

    function startshow(e) {
        if (div.innerText.length == 0 && ttip.self != null && ttip.shown) {
            ttip.self.hide();
            return;
        }

        div.style.left = e.clientX + "px";
        div.style.top = e.clientY + "px";
        let coordx = e.clientX;
        let coordy = e.clientY;
        let dw = div.clientWidth;
        let dh = div.clientHeight;

        if (coordx + dw > window.innerWidth)
            div.style.left = (coordx - (dw + 24)) + "px";

        if (coordy + dh > window.innerHeight)
            div.style.top = (coordy - (dh + 24)) + "px";

        if (!ttip.shown && ttip.self != null)
            ttip.self.show(e);
        else {
            clearTimeout(ttip.timeout);
            ttip.timeout = setTimeout(this.hide, 15000);
        }
    }

    if (ttip.text != text && ttip.self != null) {
        ttip.self.hide();
    }


    if (ttip.self != null)
        return;

    ttip.text = text;
    div.innerHTML = text;
    ttip.self = this;
    ttip.shown = false;

    owner.addEventListener("mousemove", startshow);
  }


function cutherefound(newdiv, list, index)
{
    let adder = 0;
    do {
        drawoneline(newdiv, list[index + adder], true);
        newdiv.innerHTML += "<br>";
        adder += 1;
    } while (!list[index + adder - 1].includes("end trace"));
    return {add:adder, elem:newdiv}

}

/**
 * main handler
 */
function ruler(list, index)
{
    let newdiv = document.createElement('div');
    newdiv.className = "innerdiv";
    newdiv.style.setProperty('--div-background-color', highlighteri == 1 ? '#d1d1b1' : '');

    let parts = getlineparts(list[index]);

    if (list[index].includes("------------[ cut here ]------------")) {
        try {
            let item = cutherefound(newdiv, list, index);
            if (generalprocessingblock == false) {
                stateperline.push({...state});

                let n = (stateperline.length - 1);
                for (var i = 0; i < item["elem"].childNodes.length; i++) {
                    if (item["elem"].childNodes[i].className == "innerspan") {
                        item["elem"].childNodes[i].addEventListener('dblclick', function (event) {

                        let txt = formatcontent(sortObject(stateperline[n]));

                        stateid.innerHTML = txt;
                        modal.style.display = "block";
                    });
                    }
                }
            }
            return item;
        } catch (err) {};
    }

    if (Object.keys(window).includes(parts[2])) {
        try {
            let item = eval(parts[2])(newdiv, list, index);
            if (generalprocessingblock == false) {

                stateperline.push({...state});

                let n = (stateperline.length - 1);
                for (var i = 0; i < item["elem"].childNodes.length; i++) {
                    if (item["elem"].childNodes[i].className == "innerspan") {
                        item["elem"].childNodes[i].parentElement.addEventListener('mousemove', function (event) {
                            if (ttip.self == null)
                                new tooltipping(this.parentElement.parentElement, formatcontent(sortObject(stateperline[n])));
                            else
                                ttip.text = formatcontent(sortObject(stateperline[n]));
                        });
                    }
                }
            }
            return item;
        } catch (err) {};
    }

    let item = {add: 1, elem: eval(drawoneline(newdiv, list[index]))};
    if (generalprocessingblock == false) {

        stateperline.push({...state});

        let n = (stateperline.length - 1);

        for (var i = 0; i < item["elem"].childNodes.length; i++) {
            if (item["elem"].childNodes[i].className == "innerspan") {

                item["elem"].childNodes[i].parentElement.addEventListener('mousemove', function (event) {
                    if (ttip.self == null)
                        new tooltipping(this.parentElement.parentElement, formatcontent(sortObject(stateperline[n])));
                    else
                        ttip.text = formatcontent(sortObject(stateperline[n]));
                });
            }
        }
    }
    return item;
}

function adjustmessages() {
    lasttime = 0.0;
    dmesgier(dmesgdivi);

    const divlist = document.getElementsByClassName('innerdiv');
    const color = highlighteri==1? '#d1d1b1': '';

    for (var n = 0; n < divlist.length; n++)
        divlist[n].style.setProperty('--div-background-color', color);
}

function partsvalid(parts) {
    if (parts.length < 2)
        return false;

    if (!(/^<.*>$/).test(parts[0]))
        return false;

    return true;
}

function drawoneline(newdiv, line, ignoreformatting=false) {
    let parts = line.split(' ');
    let span = document.createElement('span');

    if (!partsvalid(parts) || generalprocessingblock) {
        span = document.createElement('span');
        span.className = "innerspan";
        span.innerHTML = highlighter(escapeHtml(line));
        newdiv.appendChild(span);
        return newdiv;
    }

    let time = parseFloat(parts[1].slice(1, -1));
    let delta = time - lasttime;

    if (timeshow%3 == 0) {
        delta = time;
    }

    lasttime = time;

    if (messagetoggle&1 == 1 && ignoreformatting != true) {
        if (parts[2] == "i915" && parts[4][0] != '\[') {
            parts.splice(2, 2);
        } else {
            let formatparts = getlineparts(line);

            if (formatparts.length > 2 && (formatparts[2].startsWith("drm_") || formatparts[2].startsWith("intel_") || parts[2] == "i915" || parts[2].startsWith("[drm:"))) {
                formatparts[2] = "<div class=\"sdiv\"><b class='bstyle'> " + formatparts[2] + "</b></div>";

                parts = formatparts;
            }
        }
    }

    // thread
    let tmp = parts[0].slice(1, -1);
    span.innerHTML = "<" + tmp + "> [";
    span.className = "innerspan";

    if (delta >= 0 && timeshow%3 != 0)
        span.innerHTML += "+";

    span.innerHTML += delta.toLocaleString('en', {minimumIntegerDigits:1,minimumFractionDigits:6,useGrouping:false}) + "]";
    newdiv.appendChild(span);

    // rest of the line
    span = document.createElement('span');
    span.className = "innerspan";
    if (typeof dmesgwarningline != 'undefined' && line.includes(dmesgwarningline[dmesgwarninglinecurrent])) {
        span.style.backgroundColor = '#faa';
        span.id = "dmesgwarning";
        dmesgwarninglinecurrent++;
    }
    span.innerHTML = highlighter(" " + parts[2] + " " + escapeHtml(parts.slice(3).join(' ')));
    newdiv.appendChild(span);
    return newdiv;
}

function dmesgier(divvi) {
    let lines = flail.split('\n');
    dmesgwarninglinecurrent = 0;

    /*
     * Too big dmesg will cause troubles with memory, those will be just
     * put onto html without anything extra.
     */
    if (lines.length > 100000 || allowprocessing == 0) {
        let newdiv = document.createElement('div');
        generalprocessingblock = true;

        if (typeof dmesgwarningline == 'undefined') {
            newdiv.innerText = flail;
        } else {
            let worktext = escapeHtml(flail);
            let i = 0;

            return new Promise((resolve) => {
                const nIntervId = setInterval(() => {
                    for (var n = 0; n < 50 && i < dmesgwarningline.length; i ++, n++) {
                        let escapedhtml = escapeHtml(dmesgwarningline[i]);
                        worktext = worktext.replace(escapedhtml, "<span id=\"dmesgwarning\" class=\"innerspan\" style=\"background-color:#faa;\">" + escapedhtml + "</span>");
                    }

                    if ( i >= dmesgwarningline.length) {
                        clearInterval(nIntervId);
                        newdiv.innerHTML = worktext;


                        newdiv.className = "innerspan";

                        if (dmesgdivi == null)
                            divvi.appendChild(newdiv);
                        else {
                            divvi.replaceWith(newdiv);
                        }

                        resolve();
                    }
                }, 1);
            });
        }

        newdiv.className = "innerspan";

        if (dmesgdivi == null)
            divvi.appendChild(newdiv);
        else {
            divvi.replaceWith(newdiv);
        }

        dmesgdivi = newdiv;
        return new Promise((resolve) => {resolve()});
    } else
        generalprocessingblock = false;

    let newdiv = document.createElement('div');
    stateperline = [];
    state = {};
    var add = 1;
    var i = 0;

    return new Promise((resolve) => {
        const nIntervId = setInterval(() => {
            for (var n = 0; n < 5000 && i < lines.length; i += add, n += add) {
                var item = ruler(lines, i);

                /*
                 * Item will contain two variables:
                 * add: how many lines were advanced in dmesg
                 * elem: html element source to be added
                 */
                if (item["elem"] != null)
                    newdiv.appendChild(item["elem"]);

                add = item["add"];
            }

            if (i >= lines.length) {
                if (dmesgdivi == null)
                    divvi.appendChild(newdiv);
                else {
                    divvi.replaceWith(newdiv);
                }

                divvi.disabled = false;
                dmesgdivi = newdiv;
                clearInterval(nIntervId);
                resolve();
            }
        }, 1);
    });
}

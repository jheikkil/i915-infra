/**
 * kms
 */
function intel_modeset_setup_hw_state(newdiv, list, index)
{
    let adder = 0;

    do {
        drawoneline(newdiv, list[index + adder]);
        newdiv.innerHTML += "<br>";
        adder += 1;
    } while (getlineparts(list[index + adder])[2] == "intel_modeset_setup_hw_state");

    return {add:adder, elem:newdiv}
}

function drm_helper_probe_single_connector_modes(newdiv, list, index)
{
    let parts = getlineparts(list[index]);
    let key = parts.find(first => first.includes("CONNECTOR:"));

    if (parts[parts.length-1] == "disconnected")
        state[key] = "disconnected";
    else if (parts[4] == "probed")
        state[key] = "connected";

    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function pipe_config_mismatch(newdiv, list, index)
{
    let adder = 0;

    do {
        drawoneline(newdiv, list[index + adder]);
        newdiv.innerHTML += "<br>";
        adder += 1;
    } while (getlineparts(list[index + adder])[2] == "pipe_config_mismatch");
    return {add:adder, elem:newdiv}
}

function skl_compute_wm(newdiv, list, index)
{
    let adder = 0;

    do {
        let parts = getlineparts(list[index + adder]);

        let planeparts = parts[3].replace(']', '').split(":");
        if (parts[3].startsWith("PLANE") && parts[7] == "lines") {
            if (parts[67] == parts[64] && parts[43] == "0,") {
                let key = "PLANE" + parts[4].charAt(parts[4].length-2) + " " + planeparts[2] + " " + parts[4].replace(']', '');
                state[key] = "fb: [NOFB] visible: no";
            }
        }

        drawoneline(newdiv, list[index + adder]);
        newdiv.innerHTML += "<br>";
        adder += 1;
    } while (getlineparts(list[index + adder])[2] == "skl_compute_wm");
    return {add:adder, elem:newdiv}
}


var pipe_config_key = "ERROR";
function intel_dump_pipe_config(newdiv, list, index)
{
    let adder = 0;
    let parts = getlineparts(list[index]);
    var key = pipe_config_key;

    do {
        if (parts[3].startsWith("CRTC:")) {
            key = parts.slice(3, 5).join(" ");
            key = key.replace(/:.*:/, ' ');
            pipe_config_key = key;

            if (parts[5] == "enable:")
                state[key] = parts.slice(5, parts.length).join(" ");
        } else if (parts[3].startsWith("active:")) {
            state[key] += " " + parts.slice(5, 7).join(" ");
        } else if (parts[3].startsWith("PLANE:")) {
            try {
                let planekey = "PLANE" + parts[4].charAt(parts[4].length-1) + " " + parts[3].substring(parts[3].lastIndexOf(':')+1) + " " + parts[4].replace(']', '');
                state[planekey] =  parts.slice(5, parts.length).join(' ').replace(',', '');
            } catch (err){};
        } else if (parts[3] == "requested" && parts[4] == "mode:") {
            try {
                if (parts.length < 6) {
                    let partsplus = getlineparts(list[index + adder + 1]);
                    state[key] += " requested mode: " + partsplus[4].slice(1, partsplus[4].length-2) + " " + partsplus[5] + "Hz";
                } else {
                    state[key] += " requested mode: " + parts[5].slice(1, parts[5].length-2) + " " + parts[6] + "Hz";
                }
            } catch (err){};
        }

        drawoneline(newdiv, list[index + adder]);
        newdiv.innerHTML += "<br>";
        adder += 1;

        parts = getlineparts(list[index + adder]);
    } while (parts[2] == "intel_dump_pipe_config" || parts[2].startsWith("intel_dump_m_n_config") || parts[2] == "drm_mode_debug_printmodeline" || parts[2] == "intel_dump_crtc_timings"
            || parts[2] == "icl_dump_hw_state" );

    return {add:adder, elem:newdiv}
}

function intel_bw_atomic_check(newdiv, list, index)
{
    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function intel_psr_disable_locked(newdiv, list, index)
{
    let parts = getlineparts(list[index]);

    if (parts[3] == "Disabling") {
        state["OTHER " + parts[4]] = "Disabled";
    }
    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function intel_psr_enable_locked(newdiv, list, index)
{
    let parts = getlineparts(list[index]);

    if (parts[3] == "Enabling") {
        state["OTHER " + parts[4]] = "Enabled";
    }
    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function intel_dump_cdclk_config(newdiv, list, index)
{
    let parts = getlineparts(list[index]);
    state["OTHER cdclk"] = parts[6] + " kHz";

    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function intel_fbc_update(newdiv, list, index)
{
    let parts = getlineparts(list[index]);
    if (parts[3] == "Enabling")
        state["OTHER FBC Pipe " + parts[7].replace(/[^a-z]/gi, '')] = "Enabled";

    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function __intel_fbc_disable(newdiv, list, index)
{
    let parts = getlineparts(list[index]);
    if (parts[3] == "Disabling")
        state["OTHER FBC Pipe " + parts[7].replace(/[^a-z]/gi, '')] = "Disabled";

    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

function atomic_remove_fb(newdiv, list, index)
{
    let parts = getlineparts(list[index]);
    let key = "PLANE" + parts[5].charAt(parts[5].length-1) + " " + parts[4].slice(parts[4].lastIndexOf(":")+1) + " " + parts[5];
    state[key] = "[NOFB] because " + parts[7]+ " is removed";

    return {add:1, elem:eval(drawoneline(newdiv, list[index]))}
}

